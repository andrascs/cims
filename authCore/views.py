from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserSerializer, UserSerializerWithToken
from django.views.generic import TemplateView

#<-----------USER---------->
@api_view(['GET'])
def current_user(request):
    # Determine the current user by their token, and return their data
    # React will check if the user has a token stored in the browser, and if a token is found, it’ll make a request to this view. 
    serializer = UserSerializer(request.user)
    return Response(serializer.data)

class UserList(APIView):
    # Create a new user. It's called 'UserList' because normally we'd have a get
    # method here too, for retrieving a list of all User objects.
    # The serializer checks whether or not the data is valid, and if it is, 
    # it’ll save the new user and return that user’s data in the response 
    permission_classes = (permissions.AllowAny,)
    # because otherwise, the user would have to be logged in before they could sign up

    def post(self, request, format=None):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#<------------FILE UPLOAD------->
# class Home(TemplateView):
#     template_name = 'home.html'

# def upload(request):
#     return render(request, 'upload.html')